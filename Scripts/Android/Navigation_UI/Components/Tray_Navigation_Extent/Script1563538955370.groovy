import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.relevantcodes.extentreports.ExtentTest
import com.relevantcodes.extentreports.LogStatus

import groovy.json.JsonSlurper
import internal.GlobalVariable

ExtentTest extentTest = extentTest

Mobile.callTestCase(findTestCase('Android/Navigation_UI/Components/Json_Response_String'), [('tab'):'home'], FailureHandling.STOP_ON_FAILURE)

String jsonString = GlobalVariable.G_JsonString

'Parsing the Json Response and fetching the layout type'
extentTest.log(LogStatus.INFO, 'Parsing the Json Response and fetching the layout type')
JsonSlurper slurper = new JsonSlurper()
Map parsedJson = slurper.parseText(jsonString)

int iTotalCount = parsedJson.total_items


for(int i=0; i< iTotalCount; i++){
	String iTray = parsedJson.assets[i].trayLayout
	//println iTray
	
		if(iTray.equalsIgnoreCase(TrayLayoutType)){
			extentTest.log(LogStatus.INFO, 'Navigating to the tray title based on the layout type ' + TrayLayoutType)
			String trayTitle = parsedJson.assets[i].title
			//println trayTitle			
			'Navigating to the tray title based on the layout type'			
			CustomKeywords.'newpackage.swipe.swipeUp'(trayTitle)
			GlobalVariable.G_Run_Time_Index = i
			break					
		}
		
}

//extentTest.log(LogStatus.INFO, 'Navigated to the tray title ' + trayTitle + " of the layout type " + TrayLayoutType)