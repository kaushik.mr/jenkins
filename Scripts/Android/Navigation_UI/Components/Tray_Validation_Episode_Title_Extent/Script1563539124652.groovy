import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.relevantcodes.extentreports.ExtentTest as ExtentTest
import com.relevantcodes.extentreports.LogStatus as LogStatus
import groovy.json.JsonSlurper as JsonSlurper
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

//def response = WS.sendRequest(findTestObject('Android/Navigation_UI/img_thumbnail/baseURL', [('tab'):'home']))
//WS.verifyResponseStatusCode(response, 200)
//
ExtentTest extentTest = extentTest

String jsonString = GlobalVariable.G_JsonString

JsonSlurper slurper = new JsonSlurper()

Map parsedJson = slurper.parseText(jsonString)

int iRun_Time_Index = GlobalVariable.G_Run_Time_Index

println(iRun_Time_Index)

List<?> jsonResponseTitle = parsedJson.assets[iRun_Time_Index].assets[0].items

extentTest.log(LogStatus.INFO, 'Validating the API Data with UI Data on the Voot Homepage')

for (int j = 0; j < jsonResponseTitle.size(); j++) {
    String episodeTitle = parsedJson.assets[iRun_Time_Index].assets[0].items[j].title

    'Validating the API Data with UI Data on the Voot Homepage'
    CustomKeywords.'utilityFunctions.GenericMethods.VerifyAPIMetaDataWithUI'(findTestObject('Android/Navigation_UI/Episode_Tray/Episode_Title', 
            [('Episode_Title') : episodeTitle]), episodeTitle, extentTest, 2)

    int device_Width = Mobile.getDeviceWidth()

    CustomKeywords.'newpackage.swipe.swipeLeft'()
}

