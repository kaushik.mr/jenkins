import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.relevantcodes.extentreports.ExtentReports as ExtentReports
import com.relevantcodes.extentreports.ExtentTest as ExtentTest
import com.relevantcodes.extentreports.LogStatus as LogStatus
import internal.GlobalVariable as GlobalVariable
import io.appium.java_client.MobileDriver as MobileDriver
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

String execID = (RunConfiguration.getExecutionSourceName() + '_') + GlobalVariable.ExecutionID

ExtentReports extent = CustomKeywords.'com.katalon.plugin.keyword.extentReport.Extent.setupExtentReport'(execID)

String tcID = GlobalVariable.CurrentID

ExtentTest extentTest = CustomKeywords.'com.katalon.plugin.keyword.extentReport.Extent.startExtentTest'(tcID, 'Trays Validation', 
    extent)

'Navigating the Voot application'
extentTest.log(LogStatus.INFO, 'Invoking the Voot application')

'Start Voot Application'
String filePath = RunConfiguration.getProjectDir() + '/build/Voot_2.1.72_255_RC_GE.apk'

Mobile.startApplication(filePath, false)

Mobile.delay(3)

CustomKeywords.'newpackage.swipe.swipeUpWORef'()

MobileDriver driver = MobileDriverFactory.getDriver()

extentTest.log(LogStatus.INFO, 'Logging in as a Guest User')

Mobile.tap(findTestObject('Android/Login/btn_SkipAndExplore'), 10)

extentTest.log(LogStatus.INFO, 'Selecting the channel from the split screen')

Mobile.tap(findTestObject('Android/Login/btn_Voot'), 10)

Mobile.waitForElementPresent(findTestObject('Android/Login/btn_Menu'), 10)

'Verify Traylayout showsPoster'
Mobile.callTestCase(findTestCase('Android/Navigation_UI/Components/Tray_Navigation_Extent'), [('TrayLayoutType') : 'showsPoster'
        , ('extentTest') : extentTest], FailureHandling.STOP_ON_FAILURE)

'Verify the metadata of the tray on the home page and on clicking the more button'
Mobile.callTestCase(findTestCase('Android/Navigation_UI/Components/Tray_Validation_Episode_Title_Extent'), [('extentTest') : extentTest], 
    FailureHandling.CONTINUE_ON_FAILURE)

driver.quit()

extentTest.log(LogStatus.INFO, 'Application closed')

CustomKeywords.'com.katalon.plugin.keyword.extentReport.Extent.tearDownTest'(extent, extentTest)



