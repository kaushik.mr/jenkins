package api

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.text.SimpleDateFormat

import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

public class APIData {

	@Keyword

	def List<String> APICarousalTrayArray(){
		List<String> aAPITitleMetaData = new ArrayList<String>()
		List<String> aAPILabelMetaData = new ArrayList<String>()
		List<String> aAPIDescMetaData = new ArrayList<String>()
		List<String> aAPIMediaTypeMetaData = new ArrayList<String>()
		List<String>[] aAPICarousal = new List[4];
		List<String> aAPICarousalNew = new ArrayList<String>();
		def response = WS.sendRequest(findTestObject('Android/Navigation_UI/carousal_Tray/homePageAPI'))

		WS.verifyResponseStatusCode(response, 200)

		//println(response.getResponseText())

		String jsonString = response.getResponseText()

		JsonSlurper slurper = new JsonSlurper()
		Map parsedJson = slurper.parseText(jsonString)

		int iTotalCount = parsedJson.total_items

		for(int i=0; i< iTotalCount; i++){
			String iTray = parsedJson.assets[i].trayId
			if(iTray.equalsIgnoreCase('mastHeadTray')){
				int iCarousalCount = parsedJson.assets[i].offset
				for(int j=0; j < iCarousalCount; j++){

					String Carousaltitle = parsedJson.assets[i].assets[0].items[j].title
					aAPITitleMetaData.add(Carousaltitle)
					String CarousalLabel = parsedJson.assets[i].assets[0].items[j].label
					aAPILabelMetaData.add(CarousalLabel)
					String CarousalDesc = parsedJson.assets[i].assets[0].items[j].desc
					if(CarousalDesc.contains('\"')){
						CarousalDesc = CarousalDesc.replaceAll("\"","")
						String[] aCarousal = CarousalDesc.split(",")
						aAPIDescMetaData.add(aCarousal[0])
						//println aCarousal[0]
					}else{
						aAPIDescMetaData.add(CarousalDesc)
					}

					String MediaType = parsedJson.assets[i].assets[0].items[j].mediaType
					aAPIMediaTypeMetaData.add(MediaType)
				}
				aAPICarousal[0] = aAPITitleMetaData;
				aAPICarousal[1] = aAPILabelMetaData;
				aAPICarousal[2] = aAPIDescMetaData;
				aAPICarousal[3] = aAPIMediaTypeMetaData;

				for (int k = 0; k < aAPICarousal.length; k++) {
					aAPICarousalNew.add(aAPICarousal[k]);
				}

				return aAPICarousalNew
				break;
			}
		}

	}

	@Keyword

	def List<String> APITrayArray(){
		List<String> aAPITrayTitle = new ArrayList<String>()
		List<String> aAPILabelMetaData = new ArrayList<String>()
		List<String> aAPIDescMetaData = new ArrayList<String>()
		List<String> aAPIMediaTypeMetaData = new ArrayList<String>()
		List<String>[] aAPICarousal = new List[4];
		List<String> aAPICarousalNew = new ArrayList<String>();
		def response = WS.sendRequest(findTestObject('Android/Navigation_UI/carousal_Tray/homePageAPI'))

		WS.verifyResponseStatusCode(response, 200)

		//println(response.getResponseText())

		String jsonString = response.getResponseText()

		JsonSlurper slurper = new JsonSlurper()
		Map parsedJson = slurper.parseText(jsonString)

		int iTotalCount = parsedJson.total_items

		for(int i=0; i< iTotalCount; i++){
			String iTray = parsedJson.assets[i].trayId
			if(iTray.equalsIgnoreCase('mastHeadTray')){
				int iCarousalCount = parsedJson.assets[i].offset
				for(int j=0; j < iCarousalCount; j++){

					String Carousaltitle = parsedJson.assets[i].assets[0].items[j].title
					aAPITitleMetaData.add(Carousaltitle)
					String CarousalLabel = parsedJson.assets[i].assets[0].items[j].label
					aAPILabelMetaData.add(CarousalLabel)
					String CarousalDesc = parsedJson.assets[i].assets[0].items[j].desc
					if(CarousalDesc.contains('\"')){
						CarousalDesc = CarousalDesc.replaceAll("\"","")
						String[] aCarousal = CarousalDesc.split(",")
						aAPIDescMetaData.add(aCarousal[0])
						//println aCarousal[0]
					}else{
						aAPIDescMetaData.add(CarousalDesc)
					}

					String MediaType = parsedJson.assets[i].assets[0].items[j].mediaType
					aAPIMediaTypeMetaData.add(MediaType)
				}
				aAPICarousal[0] = aAPITitleMetaData;
				aAPICarousal[1] = aAPILabelMetaData;
				aAPICarousal[2] = aAPIDescMetaData;
				aAPICarousal[3] = aAPIMediaTypeMetaData;

				for (int k = 0; k < aAPICarousal.length; k++) {
					aAPICarousalNew.add(aAPICarousal[k]);
				}

				return aAPICarousalNew
				break;
			}
		}

	}

	@Keyword
	def String getsbuLabel(String sbuid){
		def responseConfigAPI = WS.sendRequest(findTestObject('Android/Navigation_UI/Episode_Tray/Config'))
		//WS.verifyResponseStatusCode(responseNextPageURL, 200)
		String jsonStringConfig = responseConfigAPI.getResponseText()

		if(responseConfigAPI.statusCode == 200){
			JsonSlurper slurper = new JsonSlurper()

			Map parsedJsonConfig = slurper.parseText(jsonStringConfig)
			List<?> jsonResponseTitle = parsedJsonConfig.assets.SBU_LIST

			for (int j = 0; j < jsonResponseTitle.size(); j++) {
				String SbuID = parsedJsonConfig.assets.SBU_LIST[j].sbuId
				if(SbuID.contentEquals(sbuid)){
					String sbuLabel = parsedJsonConfig.assets.SBU_LIST[j].sbuLabel
					return sbuLabel
					break
				}
			}
		}
	}

	@Keyword
	def boolean IsPlayableContent(TestObject to, int mediaID, int timeout){
		if(mediaID == 391||mediaID == 420||mediaID == 390||mediaID == 419){
			try {
				WebElement element = MobileElementCommonHelper.findElement(to, timeout)
				if (element != null) {
					KeywordUtil.markPassed("Object " + to.getObjectId() + " is present")
				}
				return true
			} catch (Exception e) {
				KeywordUtil.markFailed("Object " + to.getObjectId() + " is not present")
			}
			return false;
		}
	}

	@Keyword
	def boolean IsNotPlayableContent(TestObject to, int mediaID, int timeout){
		if(mediaID == 389||mediaID == 421){
			try {
				WebElement element = MobileElementCommonHelper.findElement(to, timeout)
				if (element == null) {
					KeywordUtil.markPassed("Object " + to.getObjectId() + " is not present")
				}
				return true
			} catch (Exception e) {
				KeywordUtil.markFailed("Object " + to.getObjectId() + " is present")
			}
			return false;
		}
	}

	@Keyword
	def boolean VerifyAPIMetaDataWithUI(TestObject to, String APIData, int timeout){
		WebElement element = MobileElementCommonHelper.findElement(to, timeout)
		String ActualResult
		if((element != null)){
			ActualResult = element.getText()
		}else{
			ActualResult = ''
		}

		if (APIData.equalsIgnoreCase(ActualResult)) {
			KeywordUtil.markPassed("API Data: "+ APIData + " matches UI Data: " + ActualResult)
			return true
		}else{
			KeywordUtil.markFailed("API Data: "+ APIData + " does not match UI Data: " + ActualResult)
			Mobile.takeScreenshot()
		}
		return false;
	}

	@Keyword
	def VerifyMetaDataSmallTray(int iRun_Time_Index){
		List all =[]
		def response = WS.sendRequest(findTestObject('Android/Navigation_UI/img_thumbnail/baseURL', [('tab'):'home']))
		WS.verifyResponseStatusCode(response, 200)
		String jsonString = GlobalVariable.G_JsonString
		JsonSlurper slurper = new JsonSlurper()
		Map parsedJson = slurper.parseText(jsonString)
		//		 int iRun_Time_Index = GlobalVariable.G_Run_Time_Index
		println(iRun_Time_Index)
		List<?> jsonResponseTitle = parsedJson.assets[iRun_Time_Index].assets[0].items

		for (int j = 0; j < jsonResponseTitle.size(); j++) {
			String seriesTitle = parsedJson.assets[iRun_Time_Index].assets[0].items[j].title
			String ep_no = parsedJson.assets[iRun_Time_Index].assets[0].items[j].episodeNo
			String ep_season = parsedJson.assets[iRun_Time_Index].assets[0].items[j].season
			String episode_season =('S0'+ep_season)
			println(episode_season)
			String ep_duration = parsedJson.assets[iRun_Time_Index].assets[0].items[j].duration
			int a = ep_duration.toInteger()
			int episode_duration = (a/1000)/60
			print(episode_duration)
			String episode_tcd = parsedJson.assets[iRun_Time_Index].assets[0].items[j].telecastDate
			String date1 = new SimpleDateFormat("yyyyMMdd").parse(episode_tcd)
			println(date1)
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy")
			String strDate= formatter.format(date1)
			println(strDate)
			String meta_data
			if(ep_no.length()<2){
				String episode_number ='E0'+ep_no
				meta_data = episode_season+' '+episode_number+strDate+'  |  '+episode_duration+'m'
			}
			else if(ep_no.length()==2){
				String episode_number ='E'+ep_no
				meta_data = episode_season+' '+episode_number+strDate+'  |  '+episode_duration+'m'
			}
			all.add[meta_data]
		}
		return all
	}
}