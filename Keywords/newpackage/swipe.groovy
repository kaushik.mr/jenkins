package newpackage
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.model.FailureHandling

import io.appium.java_client.AppiumDriver
import io.appium.java_client.TouchAction
import static io.appium.java_client.touch.offset.PointOption.point;

public class swipe {
	@Keyword
	def swipeUp(String Show_type) {
		int device_Width = Mobile.getDeviceWidth()
		int device_Height = Mobile.getDeviceHeight()

		while(!(Mobile.verifyElementExist(findTestObject('Android/Navigation_UI/NavigationUIS/voot_Showstabobjects/title_ShowsWeRecommend',[('Show_type') : Show_type]), 5, FailureHandling.OPTIONAL))) {
			int startX = device_Width / 2
			int startY = device_Height * 0.4
			int endX = startX
			int endY = device_Height * 0.1
			Mobile.swipe(startX, startY, endX, endY)
		}
		println device_Width
		AppiumDriver<?> driver = MobileDriverFactory.getDriver();
		TouchAction touchAction = new TouchAction(driver);
		if(device_Width==1440){
			int midY = (device_Height / 2)-265
			String PosX = Mobile.getAttribute(findTestObject('Android/Navigation_UI/NavigationUIS/voot_Showstabobjects/title_ShowsWeRecommend',[('Show_type') : Show_type]), 'x', 5)
			int currentPosX = Integer.parseInt(PosX)
			String PosY = Mobile.getAttribute(findTestObject('Android/Navigation_UI/NavigationUIS/voot_Showstabobjects/title_ShowsWeRecommend',[('Show_type') : Show_type]), 'y', 5)
			int currentPosY = Integer.parseInt(PosY)
			touchAction.longPress(point(currentPosX, currentPosY)).moveTo(point(currentPosX, midY)).release().perform()
		}
		else if(device_Width==720){
			int midY = (device_Height / 2)-230
			String PosX = Mobile.getAttribute(findTestObject('Android/Navigation_UI/NavigationUIS/voot_Showstabobjects/title_ShowsWeRecommend',[('Show_type') : Show_type]), 'x', 5)
			int currentPosX = Integer.parseInt(PosX)
			String PosY = Mobile.getAttribute(findTestObject('Android/Navigation_UI/NavigationUIS/voot_Showstabobjects/title_ShowsWeRecommend',[('Show_type') : Show_type]), 'y', 5)
			int currentPosY = Integer.parseInt(PosY)
			touchAction.longPress(point(currentPosX, currentPosY)).moveTo(point(currentPosX, midY)).release().perform()
		}
		else{
			int midY = (device_Height / 2)-200
			String PosX = Mobile.getAttribute(findTestObject('Android/Navigation_UI/NavigationUIS/voot_Showstabobjects/title_ShowsWeRecommend',[('Show_type') : Show_type]), 'x', 5)
			int currentPosX = Integer.parseInt(PosX)
			String PosY = Mobile.getAttribute(findTestObject('Android/Navigation_UI/NavigationUIS/voot_Showstabobjects/title_ShowsWeRecommend',[('Show_type') : Show_type]), 'y', 5)
			int currentPosY = Integer.parseInt(PosY)
			touchAction.longPress(point(currentPosX, currentPosY)).moveTo(point(currentPosX, midY)).release().perform()
		}
	}
	@Keyword
	def swipeUpChannelstab(String Channels_label) {
		int device_Width = Mobile.getDeviceWidth()
		int device_Height = Mobile.getDeviceHeight()

		while(!(Mobile.verifyElementExist(findTestObject('Android/Navigation_UI/NavigationUIS/voot_ChannelstabOR/label_ChannelCNNNews18',[('Channels_label') : Channels_label]), 5, FailureHandling.OPTIONAL))) {
			int startX = device_Width / 2
			int startY = device_Height * 0.4
			int endX = startX
			int endY = device_Height * 0.2
			Mobile.swipe(startX, startY, endX, endY)
		}
	}

	@Keyword
	def swipeLeft() {
		int device_Width = Mobile.getDeviceWidth()
		int device_Height = Mobile.getDeviceHeight()
		println(device_Width)
		int startX
		int endX
		if(device_Width == 1440){
			startX = device_Width * 0.6
			endX = device_Width * 0.2
		}else{
			startX = device_Width * 0.7
			endX = device_Width * 0.2
		}

		int startY = device_Height / 2
		Mobile.swipe(startX, startY, endX, startY)
	}

	@Keyword
	def swipeFullLeft() {
		int device_Width = Mobile.getDeviceWidth()
		int device_Height = Mobile.getDeviceHeight()

		int startX = device_Width * 0.9
		int endX = device_Width * 0.1
		int startY = device_Height / 2
		Mobile.swipe(startX, startY, endX, startY)
	}

	@Keyword
	def swipeFullRight() {
		int device_Width = Mobile.getDeviceWidth()
		int device_Height = Mobile.getDeviceHeight()

		int startX = device_Width * 0.1
		int endX = device_Width * 0.9
		int startY = device_Height / 2
		Mobile.swipe(startX, startY, endX, startY)
	}

	@Keyword
	def swipeUpWORef() {
		int device_Width = Mobile.getDeviceWidth()
		int device_Height = Mobile.getDeviceHeight()
		println(device_Height)
		int startY
		int endY
		if(device_Height == 2560){
			startY = device_Height * 0.3
			endY = device_Height * 0.15
		}else{
			startY = device_Height * 0.4
			endY = device_Height * 0.15
		}
		int startX = device_Width / 2


		Mobile.swipe(startX, startY, startX, endY)
	}

	@Keyword
	def swipeLeftSmallTray() {
		int device_Width = Mobile.getDeviceWidth()
		int device_Height = Mobile.getDeviceHeight()
		println(device_Width)
		int startX
		int endX
		int startY = device_Height / 2
		if(device_Width == 1440){
			startX = device_Width * 0.4
			endX = device_Width * 0.2
			Mobile.swipe(startX, startY, endX, startY)
		}
		else if(device_Width == 720){
			startX = device_Width * 0.4
			endX = device_Width * 0.1
			Mobile.swipe(startX, startY, endX, startY)
		}
		else{
			startX = device_Width * 0.7
			endX = device_Width * 0.2
			Mobile.swipe(startX, startY, endX, startY)
		}
	}
}
